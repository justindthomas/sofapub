use chrono::Utc;
use collection::Collection;
use glob::glob;
use minijinja::Environment;
use rocket::config::{CipherSuite, TlsConfig};
use rsa::pkcs8::{EncodePrivateKey, EncodePublicKey, LineEnding};
use rsa::{RsaPrivateKey, RsaPublicKey};
use rust_embed::RustEmbed;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::env::var;
use std::error::Error;
use std::fs::{self, File};
use std::io::prelude::*;

pub mod actor;
pub mod collection;
pub mod create;
pub mod delete;
pub mod events;
pub mod follow;
pub mod instance;
pub mod publisher;
pub mod server;
pub mod signing;
pub mod storage;
pub mod terminal;
pub mod verify;
//pub mod test;
pub mod webfinger;
pub mod websocket;

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Configuration {
    pub domain: String,
    pub username: String,
    pub display_name: String,
    pub summary: String,
    private_key: String,
    pub public_key: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    tls_private_key_path: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    tls_certificate_path: Option<String>,
    pub published: String,
}

impl Configuration {
    pub fn load_templates(&self) -> Result<Environment, Box<dyn Error>> {
        let base = format!("{}/.sofapub", var("HOME")?);

        let pattern = format!("{base}/templates/*.jinja2");
        let paths = glob(&pattern)?;

        let mut environment = Environment::new();

        for path in paths.filter_map(Result::ok) {
            if let Some(file_name) = path.file_name() {
                if let Some(file_str) = file_name.to_str() {
                    let filename_base = file_str
                        .split('.')
                        .next()
                        .ok_or("Failed to split file name")?;
                    let template_name = filename_base.to_string();
                    let template = std::fs::read_to_string(&path)?;

                    if let Err(e) = environment.add_template_owned(template_name, template) {
                        eprintln!("Failed to add template: {e}");
                    }
                }
            }
        }

        Ok(environment)
    }
}

impl From<Configuration> for Option<TlsConfig> {
    fn from(configuration: Configuration) -> Self {
        if let (Some(tls_certificate_path), Some(tls_private_key_path)) = (
            configuration.tls_certificate_path,
            configuration.tls_private_key_path,
        ) {
            Some(
                TlsConfig::from_paths(tls_certificate_path, tls_private_key_path)
                    .with_ciphers(CipherSuite::TLS_V13_SET)
                    .with_preferred_server_cipher_order(true),
            )
        } else {
            None
        }
    }
}

struct KeyPair {
    private_key: RsaPrivateKey,
    public_key: RsaPublicKey,
}

fn get_key_pair() -> KeyPair {
    let mut rng = rand::thread_rng();
    let bits = 2048;
    let private_key = RsaPrivateKey::new(&mut rng, bits).expect("failed");
    let public_key = RsaPublicKey::from(&private_key);

    KeyPair {
        private_key,
        public_key,
    }
}

fn generate_configuration(
    username: String,
    display_name: String,
    summary: String,
    domain: String,
    tls_private_key_path: Option<String>,
    tls_certificate_path: Option<String>,
) -> Configuration {
    let keys = get_key_pair();
    Configuration {
        username,
        display_name,
        domain,
        summary,
        private_key: keys
            .private_key
            .to_pkcs8_pem(LineEnding::default())
            .unwrap()
            .to_string(),
        public_key: keys
            .public_key
            .to_public_key_pem(LineEnding::default())
            .unwrap(),
        tls_private_key_path,
        tls_certificate_path,
        published: Utc::now().format("%Y-%m-%dT%H:%M:%SZ").to_string(),
    }
}

fn create_directories(base: &str) -> bool {
    fs::create_dir_all(format!("{base}/data/inbox/activities")).is_ok()
        && fs::create_dir_all(format!("{base}/data/inbox/notes")).is_ok()
        && fs::create_dir_all(format!("{base}/data/outbox/sent")).is_ok()
        && fs::create_dir_all(format!("{base}/data/outbox/failed")).is_ok()
        && fs::create_dir_all(format!("{base}/data/outbox/published")).is_ok()
        && fs::create_dir_all(format!("{base}/templates")).is_ok()
}

fn copy_template_files(base: &str) {
    #[derive(RustEmbed)]
    #[folder = "templates/"]
    struct Templates;

    for file in Templates::iter() {
        let template = Templates::get(file.as_ref());

        if let Some(template) = template {
            match File::create(format!("{base}/templates/{}", file.as_ref())) {
                Ok(mut dest) => {
                    if let Err(e) = dest.write_all(&template.data) {
                        eprintln!("Failed to copy from {:?}: {}", file.as_ref(), e);
                    }
                }
                Err(e) => eprintln!(
                    "Failed to create destination for {:?}: {}",
                    file.as_ref(),
                    e
                ),
            }
        } else {
            eprintln!("Failed to retrieve template for {:?}", file.as_ref());
        }
    }
}

fn initialize_collections(base: &str, configuration: &Configuration) {
    let empty_followers_collection = Collection {
        id: Some(format!("https://{}/followers", configuration.domain)),
        ..Default::default()
    };
    write_to_file(
        format!("{base}/data/followers.json"),
        &empty_followers_collection,
    );

    let empty_following_collection = Collection {
        id: Some(format!("https://{}/following", configuration.domain)),
        ..Default::default()
    };
    write_to_file(
        format!("{base}/data/following.json"),
        &empty_following_collection,
    );
}

fn write_to_file<T: Serialize>(path: String, content: &T) {
    let mut file = File::create(path).unwrap();
    file.write_all(&serde_json::to_string(content).unwrap().into_bytes())
        .ok();
}

pub fn create_configuration(
    username: String,
    display_name: String,
    summary: String,
    domain: String,
    tls_private_key_path: Option<String>,
    tls_certificate_path: Option<String>,
) {
    let configuration = generate_configuration(
        username,
        display_name,
        summary,
        domain,
        tls_private_key_path,
        tls_certificate_path,
    );

    let base = format!("{}/.sofapub", var("HOME").unwrap());

    if create_directories(&base) {
        copy_template_files(&base);
        initialize_collections(&base, &configuration);

        let mut sofapub = File::create(format!("{base}/sofapub.json")).unwrap();
        sofapub
            .write_all(&serde_json::to_string(&configuration).unwrap().into_bytes())
            .ok();
    }
}

pub fn load_configuration() -> Option<Configuration> {
    let base = format!("{}/.sofapub", var("HOME").unwrap());

    if let Ok(contents) = fs::read_to_string(format!("{base}/sofapub.json")) {
        serde_json::from_str::<Configuration>(&contents).ok()
    } else {
        None
    }
}

pub fn render_template(
    configuration: &Configuration,
    template_name: &str,
    ctx: HashMap<&str, String>,
) -> Result<String, String> {
    let environment = configuration
        .load_templates()
        .map_err(|_| "Failed to load templates".to_string())?;
    let template = environment
        .get_template(template_name)
        .map_err(|_| format!("Failed to get template {}", template_name))?;
    template
        .render(ctx)
        .map_err(|_| "Failed to render template".to_string())
}
