use serde_json::Value;
use std::{io, collections::HashMap};

use crate::{collection::{add_id_to_collection, remove_id_from_collection}, Configuration, render_template};

// For followers.json
pub fn add_id_to_followers(id: String) -> io::Result<()> {
    add_id_to_collection(id, "followers.json")
}

pub fn remove_id_from_followers(id: &str) -> io::Result<()> {
    remove_id_from_collection(id, "followers.json")
}

// For following.json
pub fn add_id_to_following(id: String) -> io::Result<()> {
    add_id_to_collection(id, "following.json")
}

pub fn remove_id_from_following(id: &str) -> io::Result<()> {
    remove_id_from_collection(id, "following.json")
}

pub async fn follow(configuration: &Configuration, object: String) -> Result<Value, String> {
    let actor = format!("https://{}/profile", configuration.domain);
    let mut ctx = HashMap::new();
    ctx.insert("object", object);
    ctx.insert("actor", actor);
    ctx.insert("id", "generate".to_string());
    
    let rendered = render_template(configuration, "follow", ctx)?;
    serde_json::from_str(&rendered).map_err(|_| "Failed to deserialize template".to_string())
}

pub async fn undo_follow(configuration: &Configuration, object: String, follow_id: String) -> Result<Value, String> {
    let actor = format!("https://{}/profile", configuration.domain);
    let mut ctx = HashMap::new();
    ctx.insert("follow_object", object);
    ctx.insert("follow_id", follow_id);
    ctx.insert("actor", actor);
    ctx.insert("id", "generate".to_string());

    let rendered = render_template(configuration, "undo_follow", ctx)?;
    serde_json::from_str(&rendered).map_err(|_| "Failed to deserialize template".to_string())
}

pub async fn accept(configuration: &Configuration, follow_actor: String, follow_id: String) -> Result<Value, String> {
    let actor = format!("https://{}/profile", configuration.domain);
    let mut ctx = HashMap::new();
    ctx.insert("follow_actor", follow_actor);
    ctx.insert("follow_id", follow_id);
    ctx.insert("actor", actor);
    ctx.insert("id", "generate".to_string());

    let rendered = render_template(configuration, "accept", ctx)?;
    serde_json::from_str(&rendered).map_err(|_| "Failed to deserialize template".to_string())
}
