use serde_json::Value;
use std::collections::HashMap;

use crate::{Configuration, render_template};

pub async fn delete_actor(configuration: &Configuration) -> Result<Value, String> {
    let actor = format!("https://{}/profile", configuration.domain);
    let mut ctx = HashMap::new();
    ctx.insert("actor", actor);
    ctx.insert("id", "generate".to_string());
    
    let rendered = render_template(configuration, "delete_actor", ctx)?;
    serde_json::from_str(&rendered).map_err(|_| "Failed to deserialize template".to_string())
}

pub async fn delete_note(configuration: &Configuration, id: String) -> Result<Value, String> {
    let actor = format!("https://{}/profile", configuration.domain);
    let mut ctx = HashMap::new();
    ctx.insert("actor", actor);
    ctx.insert("id", "generate".to_string());
    ctx.insert("note_id", id);
    
    let rendered = render_template(configuration, "delete_note", ctx)?;
    serde_json::from_str(&rendered).map_err(|_| "Failed to deserialize template".to_string())
}

