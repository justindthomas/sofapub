use rocket::http::Status;
use serde_json::Value;

use glob::glob;
use std::{fs::File, io::BufReader};

pub fn retrieve_messages(pattern: &str, kind: &str) -> Result<Vec<Value>, Status> {
    let entries = glob(pattern)
        .map_err(|_| Status::InternalServerError)?
        .filter_map(|path_result| path_result.ok())
        .filter_map(|path| File::open(path).ok())
        .filter_map(|file| {
            serde_json::from_reader::<BufReader<File>, Value>(BufReader::new(file)).ok()
        })
        .filter(|json| {
            json.get("id").and_then(|x| x.as_str()).is_some()
                && json
                    .get("type")
                    .and_then(|t| t.as_str())
                    .map_or(false, |t| t == kind)
        })
        .collect();

    Ok(entries)
}
