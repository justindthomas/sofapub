use serde::{Deserialize, Serialize};

use crate::Configuration;

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct PublicKey {
    id: String,
    owner: String,
    public_key_pem: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Actor {
    #[serde(rename = "@context")]
    pub context: String,

    #[serde(rename = "type")]
    pub kind: String,

    pub name: String,
    pub summary: String,
    pub id: String,
    pub preferred_username: String,
    pub inbox: String,
    pub outbox: String,
    pub public_key: PublicKey,
    pub published: String,
}

impl From<Configuration> for Actor {
    fn from(configuration: Configuration) -> Self {
        Actor {
            context: "https://www.w3.org/ns/activitystreams".to_string(),
            kind: "Person".to_string(),
            name: configuration.display_name.clone(),
            summary: configuration.summary.clone(),
            id: format!("https://{}/profile", configuration.domain),
            preferred_username: configuration.username.clone(),
            inbox: format!("https://{}/inbox", configuration.domain,),
            outbox: format!("https://{}/outbox", configuration.domain,),
            public_key: PublicKey {
                id: format!("https://{}/profile#main-key", configuration.domain),
                owner: format!("https://{}/profile", configuration.domain),
                public_key_pem: configuration.public_key,
            },
            published: configuration.published,
        }
    }
}

impl Actor {
    pub fn profile(&self) -> String {
        format!(
            "<html><body><h1>{}</h1><h2>{}</h2></body></html>",
            self.name, self.id
        )
    }
}
