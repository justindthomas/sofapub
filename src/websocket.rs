use ezsockets::ClientConfig;
use ezsockets::CloseCode;
use ezsockets::CloseFrame;
use ezsockets::Error;
use std::io::BufRead;
use url::Url;

use crate::Configuration;
use crate::actor::Actor;
use crate::events::ClientCommand;
use crate::publisher::RemoteCommand;
use crate::publisher::RemoteResponse;

#[derive(Debug)]
enum Call {
    NewLine(String),
}

struct Client {
    handle: ezsockets::Client<Self>,
    configuration: Configuration,
}

#[rocket::async_trait]
impl ezsockets::ClientExt for Client {
    type Call = Call;

    async fn on_text(&mut self, text: String) -> Result<(), ezsockets::Error> {
        if let Ok(command) = serde_json::from_str::<RemoteCommand>(&text) {
            match command.command {
                ClientCommand::GetProfile => {
                    log::info!("received message: {text}");
                    let actor = Actor::from(self.configuration.clone());
                    let response = RemoteResponse { id: command.id, object: serde_json::to_value(actor).unwrap() };
                    self.handle.text(serde_json::to_string(&response).unwrap());
                },
                ClientCommand::PostInbox(message) => {
                    log::info!("received message: {message}");
                },
                _ => ()
            }
        }
        Ok(())
    }

    async fn on_binary(&mut self, bytes: Vec<u8>) -> Result<(), ezsockets::Error> {
        log::info!("received bytes: {bytes:?}");
        Ok(())
    }

    async fn on_call(&mut self, call: Self::Call) -> Result<(), ezsockets::Error> {
        match call {
            Call::NewLine(line) => {
                if line == "exit" {
                    log::info!("exiting...");
                    self.handle.clone()
                        .close(Some(CloseFrame {
                            code: CloseCode::Normal,
                            reason: "adios!".to_string(),
                        })).await;
                    return Ok(());
                }
                log::info!("sending {line}");
                self.handle.text(line);
            }
        };
        Ok(())
    }
}

pub async fn start(configuration: Configuration) {
    let config = ClientConfig::new("ws://localhost:8086/publish");
    
    let (handle, future) = ezsockets::connect(|handle| Client { handle, configuration }, config).await;
    // rocket::tokio::spawn(async move {
    //     let stdin = std::io::stdin();
    //     let lines = stdin.lock().lines();
    //     for line in lines {
    //         let line = line.unwrap();
    //         handle.call(Call::NewLine(line));
    //     }
    // });
    future.await.unwrap();
}
