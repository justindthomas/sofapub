use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize)]
pub struct Software {
    name: String,
    version: String,
}

impl Default for Software {
    fn default() -> Self {
        const VERSION: &str = env!("CARGO_PKG_VERSION");
        const NAME: &str = env!("CARGO_PKG_NAME");

        Software {
            name: NAME.to_string(),
            version: VERSION.to_string(),
        }
    }
}

#[derive(Deserialize, Serialize)]
pub struct NodeInfo {
    version: String,
    software: Software,
    protocols: Vec<String>,
}

impl Default for NodeInfo {
    fn default() -> Self {
        NodeInfo {
            version: "1.0".to_string(),
            software: Software::default(),
            protocols: vec!["activity_pub".to_string()],
        }
    }
}
