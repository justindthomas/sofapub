use std::fs::File;
use std::io::prelude::*;
use std::net::{IpAddr, Ipv4Addr};
use std::io::{Stdin, stdin};
use std::env::var;
use serde_json::Value;

use clap::{Args, Parser, Subcommand};
#[macro_use]
extern crate rocket;

use rocket::Config;
use serde::Deserialize;
use sofapub::create::create;
use sofapub::events::EventChannels;
use sofapub::follow::{remove_id_from_following, add_id_to_following, follow, undo_follow, accept, add_id_to_followers};
use sofapub::delete::{delete_actor, delete_note};
use sofapub::{create_configuration, Configuration, terminal, websocket};
use sofapub::server::outbox;
use sofapub::signing::{maybe_signed_get, maybe_signed_post};
use sofapub::{
    load_configuration,
    server::{
        activity_pub, followers, following, inbox_post, profile, webfinger, host_meta, nodeinfo, objects,
        ManagedConfiguration,
    },
    publisher,
};

#[derive(Deserialize, Subcommand)]
#[serde(rename_all = "lowercase")]
pub enum ClientCommands {
    Follow {
        #[arg(long)]
        id: String,

        #[arg(long)]
        inbox: String,
        
        #[arg(long)]
        undo: Option<String>,
    },
    Delete {
        #[arg(long)]
        inbox: String,

        #[arg(long)]
        id: Option<String>,
    },
    Accept {
        #[arg(long)]
        inbox: String,

        #[arg(long)]
        actor: String,

        #[arg(long)]
        id: String,
    },
    Note {
        #[arg(long)]
        inbox: String,

        #[arg(long)]
        content: Option<String>
    },
    Terminal
}

#[derive(Deserialize, Args)]
#[command(args_conflicts_with_subcommands = true)]
pub struct ClientArgs {
    #[command(subcommand)]
    command: Option<ClientCommands>,
}

#[derive(Deserialize, Subcommand)]
#[serde(rename_all = "lowercase")]
pub enum Commands {
    //Test,
    Setup {
        #[arg(long)]
        display_name: String,
        
        #[arg(short, long)]
        username: String,

        #[arg(long)]
        summary: String,

        #[arg(long)]
        domain: String,

        #[arg(long)]
        tls_private_key_path: Option<String>,

        #[arg(long)]
        tls_certificate_path: Option<String>,
    },
    Server {
        #[arg(long)]
        publish: bool
    },
    Client(ClientArgs),
    Get {
        #[arg(index = 1)]
        url: String,
    },
    Post {
        #[arg(index = 1)]
        url: String,
    },
    Webfinger {
        #[arg(index = 1)]
        id: String,
    },
    Publisher,
}

#[derive(Parser)] // requires `derive` feature
#[command(name = "sofapub")]
#[command(about = "A minimally functional ActivityPub implementation", long_about = None)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[rocket::main]
async fn main() {
    let args = Cli::parse();

    match args.command {
        //Commands::Test => handle_test().await,
        Commands::Setup { display_name, username, summary, domain, tls_private_key_path, tls_certificate_path } => {
            handle_setup(display_name, username, summary, domain, tls_private_key_path, tls_certificate_path).await
        },
        Commands::Server { publish } if publish => handle_publish().await,
        Commands::Server { publish: _ } => handle_server().await,
        Commands::Client(args) => handle_client(args).await,
        Commands::Get { url } => handle_get(url).await,
        Commands::Post { url } => handle_post(url).await,
        Commands::Webfinger { id } => handle_webfinger(id).await,
        Commands::Publisher => handle_publisher().await,
    }
}

// async fn handle_test() {
//     sofapub::test::test();
// }

async fn handle_publish() {
    env_logger::init();

    if let Some(configuration) = load_configuration() {
        websocket::start(configuration).await;
    }
}

async fn handle_setup(display_name: String, username: String, summary: String, domain: String, tls_private_key_path: Option<String>, tls_certificate_path: Option<String>) {
    create_configuration(username, display_name, summary, domain, tls_private_key_path, tls_certificate_path);
    println!("Configuration created");
}

async fn handle_get(url: String) {
    if let Some(configuration) = load_configuration() {
        if let Ok(resp) = maybe_signed_get(configuration, url, false).await {
            if let Ok(text) = resp.text().await {
                println!("{text}");
            } else {
                println!("{{}}");
            }
        } else {
            println!("{{}}");
        }
    }
}

async fn send_post(configuration: Configuration, inbox: String, content: Value) -> Result<(), String> {
    let content_string = serde_json::to_string(&content).unwrap();
    match maybe_signed_post(configuration, inbox, content_string, false).await {
        Ok(resp) => {
            if let Ok(text) = resp.text().await {
                if content["id"].is_string() {
                    println!("Activity ID: {}", content["id"].as_str().unwrap());
                }
                println!("POST delivered");

                if !text.is_empty() {
                    println!("Response text: {text}");
                }

                Ok(())
            } else {
                Err("POST successful but not text received".to_string())
            }
        },
        Err(e) => Err(format!("{e:#?}"))
    }
}

async fn handle_client(args: ClientArgs) {
    if let (Some(command), Some(configuration)) = (args.command, load_configuration()) {
        match command {
            ClientCommands::Follow { id, inbox, undo } => {
                handle_client_follow(configuration, id, inbox, undo).await;
            }
            ClientCommands::Delete { id, inbox } => {
                handle_client_delete(configuration, inbox, id).await;
            }
            ClientCommands::Accept { id, inbox, actor } => {
                handle_client_accept(configuration, inbox, id, actor).await;
            }
            ClientCommands::Note { inbox, content } => {
                handle_client_note(configuration, inbox, content).await;
            }
            ClientCommands::Terminal => {
                let _ = terminal::start();
            }
        }
    }
}


async fn handle_client_note(configuration: Configuration, inbox: String, content: Option<String>) {
    let content = content.map_or_else(|| {
        std::io::read_to_string(stdin()).ok()
    }, Some);
    
    if let Some(content) = content {
        match create(&configuration, content).await {
            Ok(create_content) => {
                let processed_post = process_json(&create_content);
                let _ = send_post(configuration, inbox, processed_post).await;
            },
            Err(e) => eprintln!("Error formatting create: {e}")
        }
    }
}

async fn handle_client_follow(configuration: Configuration, id: String, inbox: String, undo: Option<String>) {
    if let Some(undo) = undo {
        if let Ok(undo_content) = undo_follow(&configuration, id.clone(), undo).await {
            let processed_undo = process_json(&undo_content);

            if send_post(configuration.clone(), inbox.clone(), processed_undo).await.is_ok() {
                // Remove the ID from the following list, since it's an "undo" operation
                if let Err(e) = remove_id_from_following(&id) {
                    eprintln!("Failed to remove ID from following list: {e:#?}");
                }
            }
        }
    } else if let Ok(follow_content) = follow(&configuration, id.clone()).await {
        let processed_follow = process_json(&follow_content);

        if send_post(configuration.clone(), inbox.clone(), processed_follow).await.is_ok() {
            // Add the ID to the following list
            if let Err(e) = add_id_to_following(id) {
                eprintln!("Failed to add ID to following list: {e:#?}");
            }
        }
    }
}

async fn handle_delete(
    delete_result: Result<Value, String>, 
    error_msg: &str, 
    configuration: &Configuration, 
    inbox: String
) {
    match delete_result {
        Ok(delete_content) => {
            let processed_delete = process_json(&delete_content);
            let _ = send_post(configuration.clone(), inbox.clone(), processed_delete).await;
        },
        Err(e) => {
            eprintln!("{}: {}", error_msg, e); 
        }
    }
}

async fn handle_client_delete(configuration: Configuration, inbox: String, id: Option<String>) {
    if let Some(id) = id {
        handle_delete(delete_note(&configuration, id).await, "Failed to create Delete Note message", &configuration, inbox).await;
    } else {
        handle_delete(delete_actor(&configuration).await, "Failed to create Delete Actor message", &configuration, inbox).await;
    }
}


async fn handle_client_accept(configuration: Configuration, inbox: String, id: String, actor: String) {
    match accept(&configuration, actor.clone(), id).await {
        Ok(accept_content) => {
            let processed_accept = process_json(&accept_content);
            if send_post(configuration.clone(), inbox.clone(), processed_accept).await.is_ok() {
                let _ = add_id_to_followers(actor);
            }
        },
        Err(e) => {
            eprintln!("Failed to create Accept message: {e}");
        }
    }
}

async fn handle_publisher() {
    env_logger::init();

    if let Some(configuration) = load_configuration() {
        let config = Config {
            tls: configuration.into(),
            port: 8086,
            address: IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)),
            ..Default::default()
        };

        let _ = rocket::custom(config)
            .attach(ManagedConfiguration::fairing())
            .attach(EventChannels::fairing())
            .mount(
                "/",
                routes![
                    publisher::publish,
                    publisher::profile,
                    publisher::inbox_post,
                    publisher::outbox,
                    publisher::objects,
                    publisher::webfinger,
                    publisher::host_meta,
                    publisher::nodeinfo,
                ],
            )
            .launch()
            .await;
    } else {
        println!("Failed to load configuration");
    }
}

async fn handle_server() {
    env_logger::init();

    if let Some(configuration) = load_configuration() {
        let config = Config {
            tls: configuration.into(),
            port: 8086,
            address: IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)),
            ..Default::default()
        };

        let _ = rocket::custom(config)
            .attach(ManagedConfiguration::fairing())
            .mount(
                "/",
                routes![
                    host_meta,
                    webfinger,
                    activity_pub,
                    profile,
                    followers,
                    following,
                    inbox_post,
                    outbox,
                    objects,
                    nodeinfo
                ],
            )
            .launch()
            .await;
    } else {
        println!("Failed to load configuration");
    }
}

async fn handle_webfinger(id: String) {
    let parts = id.split('@').collect::<Vec<&str>>();
    let url = format!(
        "https://{}/.well-known/webfinger?resource=acct:{}@{}",
        parts[2], parts[1], parts[2]
    );

    if let Ok(resp) = reqwest::get(url).await {
        if let Ok(text) = resp.text().await {
            println!("{text}");
        } else {
            println!("{{}}");
        }
    } else {
        println!("{{}}");
    }
}

async fn handle_post(inbox: String) {
    if let Ok(json) = serde_json::from_reader::<Stdin, Value>(stdin()) {
        if let Some(configuration) = load_configuration() {
            let processed_post = process_json(&json);
            let _ = send_post(configuration, inbox, processed_post).await;
        }
    }
}

fn generate_uuid_url(configuration: &Configuration) -> (String, String) {
    let uuid = uuid::Uuid::new_v4().to_string();
    (format!("https://{}/objects/{}", configuration.domain, uuid), uuid)
}

fn write_to_file(base: &str, uuid: &str, content: &Value) {
    let mut file = File::create(format!("{}/data/outbox/published/{}", base, uuid)).unwrap();
    file.write_all(&serde_json::to_string(content).unwrap().into_bytes())
        .ok();
}

fn process_and_save_id_field(json_object: &mut serde_json::Map<String, Value>, configuration: &Configuration, base: &str) {
    if let Some(id) = json_object.get("id").and_then(|x| x.as_str()) {
        if id.to_lowercase() == "generate" {
            let (url, uuid) = generate_uuid_url(configuration);
            json_object.insert("id".to_string(), Value::String(url.clone()));

            // Check and update "url" field
            if json_object.get("url").map_or(false, |x| x == &Value::String("@id".to_string())) {
                json_object.insert("url".to_string(), Value::String(url));
            }

            write_to_file(base, &uuid, &Value::Object(json_object.clone()));
        }
    }
}

fn process_json(json: &Value) -> Value {
    let base = format!("{}/.sofapub", var("HOME").unwrap());

    if let Some(configuration) = load_configuration() {
        let mut json_clone = json.clone();

        if let Some(object) = json_clone.get_mut("object").and_then(|x| x.as_object_mut()) {
            process_and_save_id_field(object, &configuration, &base);
        }

        if let Some(root) = json_clone.as_object_mut() {
            process_and_save_id_field(root, &configuration, &base);
        }

        json_clone
    } else {
        json.clone()
    }
}

