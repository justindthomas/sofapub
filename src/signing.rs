use base64::{engine::general_purpose, engine::Engine as _};
use reqwest::{Client, Response};
use rocket::http::Status;
use rsa::pkcs1v15::{SigningKey, Signature};
use rsa::signature::{RandomizedSigner, SignatureEncoding};
use rsa::{pkcs8::DecodePrivateKey, RsaPrivateKey};
use sha2::{Digest, Sha256};
use std::fmt::{self, Debug};
use std::time::SystemTime;
use url::Url;

use crate::actor::Actor;
use crate::Configuration;

#[derive(Debug, Clone)]
pub enum Method {
    Get,
    Post,
}

impl fmt::Display for Method {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Debug::fmt(self, f)
    }
}

#[derive(Clone, Debug)]
pub struct SignParams {
    pub configuration: Configuration,
    pub url: Url,
    pub body: Option<String>,
    pub method: Method,
}

pub struct SignResponse {
    pub signature: String,
    pub date: String,
    pub digest: Option<String>,
}

pub fn sign(params: SignParams) -> SignResponse {
    let digest = compute_digest(&params.body);
    let host = params.url.host().unwrap().to_string();
    let request_target = format_request_target(&params.method, &params.url);
    let date = httpdate::fmt_http_date(SystemTime::now());

    //log::debug!("SIGN {}, {host}, {request_target}, {date}", params.url);

    let actor: Actor = params.configuration.clone().into();
    let private_key = RsaPrivateKey::from_pkcs8_pem(&params.configuration.private_key).unwrap();
    let signing_key = SigningKey::<Sha256>::new(private_key);
    let structured_data = construct_structured_data(&request_target, &host, &date, &digest);
    let signature = compute_signature(&signing_key, &structured_data);
    let response_signature = format_response_signature(&actor, &signature, digest.is_some());

    SignResponse {
        signature: response_signature,
        date,
        digest,
    }
}

fn compute_digest(body: &Option<String>) -> Option<String> {
    body.as_ref().map(|body| {
        let mut hasher = Sha256::new();
        hasher.update(body.as_bytes());
        let hashed = general_purpose::STANDARD.encode(hasher.finalize());
        format!("SHA-256={}", hashed)
    })
}

fn format_request_target(method: &Method, url: &Url) -> String {
    format!(
        "{} {}",
        method.to_string().to_lowercase(),
        url.path()
    )
}

fn construct_structured_data(request_target: &str, host: &str, date: &str, digest: &Option<String>) -> String {
    if let Some(ref digest) = digest {
        format!(
            "(request-target): {}\nhost: {}\ndate: {}\ndigest: {}",
            request_target, host, date, digest
        )
    } else {
        format!(
            "(request-target): {}\nhost: {}\ndate: {}",
            request_target, host, date
        )
    }
}

fn compute_signature(signing_key: &SigningKey<Sha256>, structured_data: &str) -> Signature {
    let mut rng = rand::thread_rng();
    signing_key.sign_with_rng(&mut rng, structured_data.as_bytes())
}

fn format_response_signature(actor: &Actor, signature: &Signature, has_digest: bool) -> String {
    if has_digest {
        format!(
            "keyId=\"{}#main-key\",algorithm=\"rsa-sha256\",headers=\"(request-target) host date digest\",signature=\"{}\"",
            actor.id,
            general_purpose::STANDARD.encode(signature.to_bytes())
        )
    } else {
        format!(
            "keyId=\"{}#main-key\",algorithm=\"rsa-sha256\",headers=\"(request-target) host date\",signature=\"{}\"",
            actor.id,
            general_purpose::STANDARD.encode(signature.to_bytes())
        )
    }
}

pub async fn maybe_signed_get(
    configuration: Configuration,
    url: String,
    accept_any: bool,
) -> Result<Response, reqwest::Error> {
    let client = Client::builder();
    let client = client.user_agent("SofaPub/0.1").build().unwrap();

    let accept = if accept_any {
        "*/*"
    } else {
        "application/activity+json"
    };

    let client = {
        let body = Option::None;
        let method = Method::Get;
        let url_str = url.clone();

        if let Ok(url) = Url::parse(&url) {
            log::debug!("SIGNING REQUEST FOR REMOTE RESOURCE");
            let signature = sign(SignParams {
                configuration,
                url,
                body,
                method,
            });

            client
                .get(url_str)
                .header("Signature", &signature.signature)
                .header("Date", signature.date)
                .header("Accept", accept)
        } else {
            client.get(&url).header("Accept", accept)
        }
    };

    client.send().await
}

pub async fn maybe_signed_post(
    configuration: Configuration,
    url: String,
    body: String,
    accept_any: bool,
) -> Result<Response, Status> {
    let client = Client::builder();
    let client = client.user_agent("SofaPub/0.1").build().unwrap();

    let accept = if accept_any {
        "*/*"
    } else {
        "application/activity+json"
    };

    let url_str = url.clone();

    if let Ok(url) = Url::parse(&url) {
        let client = {
            let method = Method::Post;
            
            log::debug!("SIGNING REQUEST FOR REMOTE RESOURCE");
            let signature = sign(SignParams {
                configuration,
                url,
                body: Some(body.clone()),
                method,
            });

            
            let mut intermediate = client
                .post(url_str)
                .header("Signature", &signature.signature)
                .header("Date", signature.date)
                .header("Accept", accept)
                .header("Content-type", "application/activity+json");

            if let Some(digest) = signature.digest {
                intermediate = intermediate.header("Digest", digest);
            };
            
            intermediate.body(body)
        };

        client.send().await.map_err(|_| Status::InternalServerError)
    } else {
        Err(Status::InternalServerError)
    }
}
