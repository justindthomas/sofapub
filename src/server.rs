use rocket::{http::Status, fairing::{Fairing, Info, Kind, self}, Rocket, Build};
use rocket::serde::json::Json;
use rocket::{get, post};
use rocket::request::{FromRequest, Outcome, Request};
use crate::{actor::Actor, Configuration, webfinger::WebFinger, load_configuration, instance::NodeInfo, collection::Collection, verify::Signed, storage::retrieve_messages, signing::maybe_signed_get};
use serde_json::Value;

use std::{fs::{self, File}, io::BufReader};
use std::io::prelude::*;
use std::env::var;

#[derive(Clone)]
pub struct ManagedConfiguration {
    pub configuration: Configuration,
}

impl ManagedConfiguration {
    pub fn fairing() -> impl Fairing {
        ConfigurationFairing
    }
}

struct ConfigurationFairing;

#[rocket::async_trait]
impl Fairing for ConfigurationFairing {
    fn info(&self) -> Info {
        Info {
            name: "SofaPub Configuration",
            kind: Kind::Ignite,
        }
    }

    async fn on_ignite(&self, rocket: Rocket<Build>) -> fairing::Result {
        log::debug!("igniting Configuration");

        if let Some(configuration) = load_configuration() {
            Ok(rocket.manage(ManagedConfiguration {
                configuration
            }))
        } else {
            Err(rocket)
        }
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for ManagedConfiguration {
    type Error = String;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        if let Some(managed) = request.rocket().state::<ManagedConfiguration>() {
            Outcome::Success(managed.clone())
        } else {
            Outcome::Failure((Status::BadRequest, "ManagedConfiguration Failed".to_string()))
        }
    }
}

#[get("/following")]
pub async fn following(managed: ManagedConfiguration) -> Result<Json<Collection>, Status> {
    let id = format!("https://{}/following", managed.configuration.domain);
    let base = format!("{}/.sofapub", var("HOME").unwrap());
    
    if let Ok(contents) = fs::read_to_string(format!("{base}/data/following.json")) {
        Ok(Json(Collection::from(serde_json::from_str::<Vec<Value>>(&contents).unwrap()).id(id).clone()))
    } else {
        Err(Status::NotFound)
    }
}

#[get("/followers")]
pub async fn followers(managed: ManagedConfiguration) -> Result<Json<Collection>, Status> {
    let id = format!("https://{}/followers", managed.configuration.domain);
    let base = format!("{}/.sofapub", var("HOME").unwrap());
    
    if let Ok(contents) = fs::read_to_string(format!("{base}/data/followers.json")) {
        Ok(Json(Collection::from(serde_json::from_str::<Vec<Value>>(&contents).unwrap()).id(id).clone()))
    } else {
        Err(Status::NotFound)
    }
}

#[get("/profile", format = "application/activity+json", rank = 1)]
pub async fn activity_pub(managed: ManagedConfiguration) -> Result<Json<Actor>, Status> {
    Ok(Json(Actor::from(managed.configuration)))
}

#[get("/profile", format = "text/html", rank = 2)]
pub async fn profile(managed: ManagedConfiguration) -> Result<String, Status> {    
    Ok(Actor::from(managed.configuration).profile())
}

#[get("/.well-known/webfinger?<resource>", format = "application/jrd+json")]
pub async fn webfinger(
    managed: ManagedConfiguration,
    resource: String,
) -> Result<Json<WebFinger>, Status> {
    log::debug!("REQUESTED WEBFINGER RESOURCE: {resource} (DISREGARDED)");
    Ok(Json(WebFinger::from(managed.configuration)))
}

async fn handle_announce(configuration: Configuration, activity: &Value) {
    if let (Some(kind), Some(object)) = (activity.get("type"), activity.get("object")) {
        if let (Some(kind), Some(object)) = (kind.as_str(), object.as_str()) {
            if kind == "Announce" {
                if let Ok(note) = maybe_signed_get(configuration, object.to_string(), false).await {
                    if let Ok(note) = note.text().await {
                        write_to_inbox(note, "notes");
                    }
                }
            }
        }
    }
}

pub fn handle_create(activity: &Value) {
    if let (Some(kind), Some(object)) = (activity.get("type"), activity.get("object")) {
        if let (Some(kind), Some(object)) = (kind.as_str(), object.as_object()) {
            if kind == "Create" {
                write_to_inbox(serde_json::to_string(&object).unwrap(), "notes");
            }
        }
    }
}

pub fn write_to_inbox(activity: String, folder: &str) {
    let uuid = uuid::Uuid::new_v4().to_string();
    let base = format!("{}/.sofapub", var("HOME").unwrap());
    
    let mut file = File::create(format!("{base}/data/inbox/{folder}/{uuid}.json")).unwrap();
    file.write_all(&activity.into_bytes())
        .ok();
}

#[post("/inbox", data = "<activity>")]
pub async fn inbox_post(
    signed: Signed,
    managed: ManagedConfiguration,
    activity: String,
) -> Result<Status, Status> {
    if let Signed(true) = signed {
        if let Ok(json) = serde_json::from_str::<Value>(&activity) {
            write_to_inbox(activity, "activities");

            handle_create(&json);
            handle_announce(managed.configuration, &json).await;

            Ok(Status::Accepted)
        } else {
            log::debug!("FAILED TO DECODE JSON\n{activity:#?}");
            Err(Status::BadRequest)
        }
    } else {
        log::debug!("SIGNATURE VERIFICATION FAILED");
        Err(Status::BadRequest)
    }
}

#[get("/outbox")]
pub async fn outbox(
    managed: ManagedConfiguration
) -> Result<Json<Value>, Status> {
    let base = format!("{}/.sofapub", var("HOME").map_err(|_| Status::InternalServerError)?);
    let pattern = format!("{}/data/outbox/published/*", base);

    match retrieve_messages(&pattern, "Create") {
        Ok(entries) => {
            Ok(Json(serde_json::to_value(
                Collection::from(entries)
                    .id(format!("https://{}/outbox", managed.configuration.domain)))
                    .map_err(|_| Status::InternalServerError)?))
        },
        Err(e) => Err(e)
    }
}


#[get("/objects/<uuid>")]
pub async fn objects(uuid: String) -> Result<Json<Value>, Status> {
    let base = format!("{}/.sofapub", var("HOME").unwrap());
    let file = File::open(format!("{}/data/outbox/published/{}", base, uuid)).map_err(|_| Status::NotFound)?;
    let reader = BufReader::new(file);
    serde_json::from_reader::<BufReader<File>, Value>(reader)
        .map(Json)
        .map_err(|_| Status::NotFound)
}


#[get("/.well-known/host-meta")]
pub async fn host_meta(managed: ManagedConfiguration) -> Result<String, Status> {
    Ok(format!(r#"<?xml version="1.0" encoding="UTF-8"?><XRD xmlns="http://docs.oasis-open.org/ns/xri/xrd-1.0"><Link rel="lrdd" template="https://{}/.well-known/webfinger?resource={{uri}}" type="application/json" /></XRD>"#, managed.configuration.domain))
}

#[get("/.well-known/nodeinfo")]
pub async fn nodeinfo() -> Result<Json<NodeInfo>, Status> {
    Ok(Json(NodeInfo::default()))
}
