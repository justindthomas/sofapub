use std::{
    error::Error,
    io,
    time::{Duration, Instant},
};

use chrono::DateTime;
use crossterm::{
    event::{self, DisableMouseCapture, EnableMouseCapture, Event, KeyCode, KeyEventKind},
    execute,
    terminal::{disable_raw_mode, enable_raw_mode, EnterAlternateScreen, LeaveAlternateScreen},
};
use ratatui::{prelude::*, widgets::*};
use serde_json::Value;
use std::env::var;

use crate::storage::retrieve_messages;

struct StatefulList<T> {
    state: ListState,
    items: Vec<T>,
}

impl<T> StatefulList<T> {
    fn with_items(items: Vec<T>) -> StatefulList<T> {
        StatefulList {
            state: ListState::default(),
            items,
        }
    }

    fn next(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i >= self.items.len() - 1 {
                    0
                } else {
                    i + 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    fn previous(&mut self) {
        let i = match self.state.selected() {
            Some(i) => {
                if i == 0 {
                    self.items.len() - 1
                } else {
                    i - 1
                }
            }
            None => 0,
        };
        self.state.select(Some(i));
    }

    fn unselect(&mut self) {
        self.state.select(None);
    }
}

fn get_messages() -> Vec<Value> {
    let base = format!("{}/.sofapub", var("HOME").unwrap());
    let pattern = format!("{}/data/inbox/notes/*", base);

    if let Ok(mut messages) = retrieve_messages(&pattern, "Note") {
        messages.sort_by_key(|x| {
            DateTime::parse_from_rfc3339(
                x.get("published")
                    .expect("published key is required")
                    .as_str()
                    .unwrap(),
            )
            .unwrap()
        });

        messages.reverse();

        messages
    } else {
        vec![]
    }
}

/// This struct holds the current state of the app. In particular, it has the `items` field which is
/// a wrapper around `ListState`. Keeping track of the items state let us render the associated
/// widget with its state and have access to features such as natural scrolling.
///
/// Check the event handling at the bottom to see how to change the state on incoming events.
/// Check the drawing logic for items on how to specify the highlighting style for selected items.
struct App {
    items: StatefulList<Value>,
}

impl App {
    fn new(items: Vec<Value>) -> App {
        App {
            items: StatefulList::with_items(items),
        }
    }

    /// Rotate through the event list.
    /// This only exists to simulate some kind of "progress"
    fn on_tick(&mut self) {
        self.items.items = get_messages();
    }
}

pub fn start() -> Result<(), Box<dyn Error>> {
    // setup terminal
    enable_raw_mode()?;
    let mut stdout = io::stdout();
    execute!(stdout, EnterAlternateScreen, EnableMouseCapture)?;
    let backend = CrosstermBackend::new(stdout);
    let mut terminal = Terminal::new(backend)?;

    // create app and run it
    let tick_rate = Duration::from_millis(250);
    let app = App::new(get_messages());
    let res = run_app(&mut terminal, app, tick_rate);

    // restore terminal
    disable_raw_mode()?;
    execute!(
        terminal.backend_mut(),
        LeaveAlternateScreen,
        DisableMouseCapture
    )?;
    terminal.show_cursor()?;

    if let Err(err) = res {
        println!("{err:?}");
    }

    Ok(())
}

fn run_app<B: Backend>(
    terminal: &mut Terminal<B>,
    mut app: App,
    tick_rate: Duration,
) -> io::Result<()> {
    let mut last_tick = Instant::now();
    loop {
        terminal.draw(|f| ui(f, &mut app))?;

        let timeout = tick_rate
            .checked_sub(last_tick.elapsed())
            .unwrap_or_else(|| Duration::from_secs(0));
        if crossterm::event::poll(timeout)? {
            if let Event::Key(key) = event::read()? {
                if key.kind == KeyEventKind::Press {
                    match key.code {
                        KeyCode::Char('q') => return Ok(()),
                        KeyCode::Left => app.items.unselect(),
                        KeyCode::Down => app.items.next(),
                        KeyCode::Up => app.items.previous(),
                        _ => {}
                    }
                }
            }
        }
        if last_tick.elapsed() >= tick_rate {
            app.on_tick();
            last_tick = Instant::now();
        }
    }
}

fn ui<B: Backend>(f: &mut Frame<B>, app: &mut App) {
    // Create two chunks with equal horizontal screen space
    let chunks = Layout::default()
        .direction(Direction::Horizontal)
        .constraints([Constraint::Percentage(100)].as_ref())
        .split(f.size());

    use regex::Regex;

    fn sanitize_html(content: &str) -> String {
        let content = content.replace("<p>", "").replace("</p>", "\n");

        // Remove <span...> tags but keep the content inside
        let span_re = Regex::new(r"<span[^>]*>").unwrap();
        let content = span_re.replace_all(&content, "");

        // Remove the closing </span> tags
        let content = content.replace("</span>", "");

        // Remove <a...> tags but keep the content inside
        let a_re = Regex::new(r"<a[^>]*>").unwrap();
        let content = a_re.replace_all(&content, "");

        // Remove the closing </a> tags
        let content = content.replace("</a>", "");

        // Replace all variations of <br> with newline characters
        let br_re = Regex::new(r"<br\s*/?>").unwrap();
        let content = br_re.replace_all(&content, "\n");

        content.to_string()
    }

    fn split_content(content: &str, len: usize) -> Vec<String> {
        let mut result = Vec::new();
        let mut start = 0;

        while start < content.len() {
            let mut end = std::cmp::min(start + len, content.len());

            // Check if there's a newline before the end of the chunk
            if let Some(newline_position) = content[start..end].find('\n') {
                end = start + newline_position;
                result.push(content[start..end].trim().to_string());

                // Add an extra blank line
                result.push("".to_string());

                // Skip the newline character for the next chunk
                end += 1;
                start = end;
                continue;
            }
            // If no newline, split at the last space within the chunk
            else if let Some(last_space) = content[start..end].rfind(' ') {
                end = start + last_space;
            }

            result.push(content[start..end].trim().to_string());

            // To handle cases where a word might be longer than `len`, move to the next chunk
            if end == start {
                end = start + len;
            }

            start = end;
        }
        result
    }

    let items: Vec<ListItem> = app
        .items
        .items
        .iter()
        .map(|i| {
            let mut lines: Vec<Line> = vec![];
            if let (Some(attributed_to), Some(published), Some(content)) =
                (i.get("attributedTo"), i.get("published"), i.get("content"))
            {
                lines.push("――――――――――――――――――――――――".into());
                lines.push(attributed_to.as_str().unwrap().into());
                lines.push(
                    Span::styled(
                        published.as_str().unwrap(),
                        Style::default().fg(Color::DarkGray),
                    )
                    .into(),
                );

                lines.push("".into());
                let sanitized_content = sanitize_html(content.as_str().unwrap());
                let split_lines = split_content(&sanitized_content, 80);

                for l in split_lines.iter() {
                    lines.push(l.clone().into());
                }
                lines.push(
                    Span::styled(
                        "[R]eply [F]orward [S]tar",
                        Style::default()
                            .fg(Color::Green)
                            .bg(Color::LightGreen)
                            .add_modifier(Modifier::BOLD),
                    )
                    .into(),
                );
                lines.push("".into());
            }
            ListItem::new(lines).style(Style::default().fg(Color::Black).bg(Color::White))
        })
        .collect();

    // Create a List from all list items and highlight the currently selected one
    let items = List::new(items)
        .block(
            Block::default()
                .borders(Borders::ALL)
                .border_style(Style::new().red())
                .title("Inbox")
                .padding(Padding::new(0, 0, 0, 0))
                .style(Style::default().bg(Color::White)),
        )
        .highlight_style(Style::default().bg(Color::LightYellow));

    // We can now render the item list
    f.render_stateful_widget(items, chunks[0], &mut app.items.state);
}
