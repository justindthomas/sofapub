use rocket::tokio::sync::Mutex;
use rocket::serde::json::Json;
use rocket::tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender};
use std::fmt::Display;
use std::hash::{Hash, Hasher};
use std::sync::Arc;

use rocket::fairing::{self, Fairing, Info, Kind};
use rocket::http::Status;
use rocket::request::{FromRequest, Outcome, Request};
use rocket::{Build, Rocket};
use serde::{Deserialize, Serialize};
use dashmap::DashMap;
use strum::IntoEnumIterator;
use strum_macros::EnumIter;
use serde_json::Value;

#[derive(Clone, Debug, Deserialize, Serialize, EnumIter, Eq, PartialEq)]
pub enum ClientCommand {
    GetFollowers,
    GetFollowing,
    GetWebFinger,
    GetProfile,
    PostInbox(Value),
    GetOutbox,
}

impl Hash for ClientCommand {
    fn hash<H: Hasher>(&self, hasher:&mut H){
        self.to_string().hash(hasher);
    }    
}

impl Display for ClientCommand {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:#?}")
    }
}

// these represent ends of two different channels, not ends of the same
// channel; Websocket will have one EndpointPair and EventChannels will
// have the EndpointPair that represents the opposite ends of two channels
#[derive(Clone)]
pub struct WebsocketEndpoint {
    pub receiver: Arc<Mutex<UnboundedReceiver<ResponseSocket>>>,
    //pub sender: Arc<Mutex<UnboundedSender<Value>>>
}

#[derive(Clone)]
pub struct ProcessorEndpoint {
    //pub receiver: Arc<Mutex<UnboundedReceiver<Value>>>,
    pub sender: Arc<UnboundedSender<ResponseSocket>>
}

#[derive(Default)]
pub struct ResponseSocket {
    pub uuid: String,
    pub tx: Option<UnboundedSender<Value>>
}

impl From<UnboundedSender<Value>> for ResponseSocket {
    fn from(sender: UnboundedSender<Value>) -> Self {
        ResponseSocket {
            uuid: uuid::Uuid::new_v4().to_string(),
            tx: Some(sender)
        }
    }
}

#[derive(Clone)]
pub struct EventChannels {
    // there's no cleanup for these maps - probable something to keep an eye on
    pub websocket_endpoints: Arc<DashMap<String, DashMap<ClientCommand, WebsocketEndpoint>>>,
    pub processor_endpoints: Arc<DashMap<String, DashMap<ClientCommand, ProcessorEndpoint>>>,
}

#[derive(Debug)]
pub enum EventChannelsError {
    RemoveFailed,
    Failed
}

impl Display for EventChannelsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{self:#?}")
    }
}

impl std::error::Error for EventChannelsError {}

impl EventChannels {
    pub fn fairing() -> impl Fairing {
        EventChannelsFairing
    }

    pub fn remove(&mut self, id: String) -> Result<(),  EventChannelsError> {
        log::debug!("remove called");

        self.websocket_endpoints.remove(&id);
        self.processor_endpoints.remove(&id);

        Ok(())
    }

    pub fn subscribe(&mut self, id: String) -> Arc<DashMap<ClientCommand, WebsocketEndpoint>> {
        log::debug!("SUBSCRIBE CALLED");

        let websocket_endpoints = DashMap::new();
        let processor_endpoints = DashMap::new();

        for command in ClientCommand::iter() {
            log::debug!("Setting channel for {command:#?}");
            // channel for sending events from EventChannels to Websocket handler
            let (evt_tx, ws_rx) = unbounded_channel();

            // channel for sending events from Websocket handler to EventChannels
            //let (ws_tx, evt_rx) = unbounded_channel();
            
            websocket_endpoints.insert(command.clone(), WebsocketEndpoint { receiver: Arc::new(Mutex::new(ws_rx)) });
            processor_endpoints.insert(command.clone(), ProcessorEndpoint { sender: Arc::new(evt_tx) });
            self.websocket_endpoints.insert(id.clone(), websocket_endpoints.clone());
            self.processor_endpoints.insert(id.clone(), processor_endpoints.clone());
        }

        Arc::new(websocket_endpoints)
    }

    pub async fn command(&mut self, id: String, command: ClientCommand) -> Result<Json<Value>, Status> {
        let endpoint_command = { if let ClientCommand::PostInbox(_) = command { ClientCommand::PostInbox(serde_json::Value::Null) } else { command.clone() }};
        if let Some(id_endpoints) = self.processor_endpoints.get(&id) {
            if let Some(command_endpoints) = id_endpoints.get(&endpoint_command) {
                let endpoint = command_endpoints.sender.clone();
                let (tx, mut rx) = unbounded_channel();
                let socket = ResponseSocket::from(tx);
                
                if endpoint.send(socket).is_ok() {
                    log::debug!("Sending command");
                    match command {
                        ClientCommand::GetProfile => {
                            log::debug!("GetProfile sent");
                            let mut k = serde_json::Value::Null;
                            //let mut guard = command_endpoints.receiver.lock().await;
                            let mut tries = 500;
                            
                            while k.is_null() && tries > 0 {
                                if let Ok(x) = rx.try_recv() {
                                    k = x
                                } else {
                                    rocket::tokio::time::sleep(rocket::tokio::time::Duration::from_millis(10)).await;
                                    tries -= 1;
                                }
                            }
                            Ok(Json(k))
                        },
                        ClientCommand::PostInbox(_) => {
                            log::debug!("PostInbox sent");
                            Ok(Json(serde_json::Value::Null))
                        },
                        _ => {
                            Ok(Json(serde_json::Value::Null))
                        }
                    }
                } else {
                    Err(Status::InternalServerError)
                }
            } else {
                Err(Status::InternalServerError)
            }
        } else {
            Err(Status::InternalServerError)
        }
    }
}

struct EventChannelsFairing;

#[rocket::async_trait]
impl Fairing for EventChannelsFairing {
    fn info(&self) -> Info {
        Info {
            name: "Event Channels",
            kind: Kind::Ignite,
        }
    }

    async fn on_ignite(&self, rocket: Rocket<Build>) -> fairing::Result {
        log::debug!("igniting EventsChannel");
        Ok(rocket.manage({
            EventChannels {
                websocket_endpoints: Arc::new(DashMap::new()),
                processor_endpoints: Arc::new(DashMap::new()),
            }
        }))
    }
}

#[rocket::async_trait]
impl<'r> FromRequest<'r> for EventChannels {
    type Error = EventChannelsError;

    async fn from_request(request: &'r Request<'_>) -> Outcome<Self, Self::Error> {
        if let Some(channels) = request.rocket().state::<EventChannels>() {
            Outcome::Success(channels.clone())
        } else {
            Outcome::Failure((Status::BadRequest, EventChannelsError::Failed))
        }
    }
}

