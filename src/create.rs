use serde_json::Value;
use std::collections::HashMap;
use chrono::Utc;

use crate::{Configuration, render_template};

pub async fn create(configuration: &Configuration, content: String) -> Result<Value, String> {
    let cc = serde_json::to_string(&vec![format!("https://{}/followers", configuration.domain)]).unwrap();
    let actor = format!("https://{}/profile", configuration.domain);
    let conversation = format!("https://{}/conversation/{}", configuration.domain, uuid::Uuid::new_v4());
    let mut ctx = HashMap::new();
    ctx.insert("cc", cc);
    ctx.insert("actor", actor.clone());
    ctx.insert("attributed_to", actor);
    ctx.insert("content", content.escape_default().to_string());
    ctx.insert("conversation", conversation);
    ctx.insert("published", Utc::now().format("%Y-%m-%dT%H:%M:%SZ").to_string());
    ctx.insert("id", "generate".to_string());
    ctx.insert("url", "@id".to_string());
    
    let rendered = render_template(configuration, "create", ctx)?;
    serde_json::from_str(&rendered).map_err(|_| "Failed to deserialize template".to_string())
}
