use serde::{Deserialize, Serialize};

use crate::Configuration;

#[derive(Serialize, Deserialize, Clone, Default, Debug)]
pub struct WebFingerLink {
    pub rel: String,
    #[serde(rename = "type")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub kind: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub href: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub template: Option<String>,
}

#[derive(Serialize, Deserialize, Clone, Default, Debug)]
pub struct WebFinger {
    pub subject: String,
    pub aliases: Option<Vec<String>>,
    pub links: Vec<WebFingerLink>,
}

impl From<Configuration> for WebFinger {
    fn from(configuration: Configuration) -> Self {
        let url = format!("https://{}", configuration.domain);

        WebFinger {
            subject: format!("acct:{}@{}", configuration.username, configuration.domain),
            aliases: Option::from(vec![format!("{}/profile", url)]),
            links: vec![
                WebFingerLink {
                    rel: "http://webfinger.net/rel/profile-page".to_string(),
                    kind: Option::from("text/html".to_string()),
                    href: Option::from(format!("{}/profile", url)),
                    ..Default::default()
                },
                WebFingerLink {
                    rel: "self".to_string(),
                    kind: Option::from("application/activity+json".to_string()),
                    href: Option::from(format!("{}/profile", url)),
                    ..Default::default()
                },
            ],
        }
    }
}
