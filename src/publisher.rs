use crate::events::{EventChannels, ClientCommand, ResponseSocket};
use crate::server::{ManagedConfiguration, write_to_inbox};
use crate::{
    instance::NodeInfo, verify::Signed, webfinger::WebFinger,
};
use ezsockets::ClientConfig;
use rocket::serde::json::Json;
use rocket::http::Status;
use rocket::{get, post};
use serde::{Deserialize, Serialize};
use serde_json::Value;
use ws::{WebSocket, Channel, Message};
use rocket::futures::{StreamExt, SinkExt};
use dashmap::DashMap;
use std::sync::Arc;
use rocket::tokio::sync::mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender};

#[derive(Serialize, Deserialize)]
pub struct RemoteCommand {
    pub id: String,
    pub command: ClientCommand
}

#[derive(Serialize, Deserialize, Debug)]
pub struct RemoteResponse {
    pub id: String,
    pub object: Value
}

#[get("/publish")]
pub async fn publish(mut events: EventChannels, ws: WebSocket) -> Channel<'static> {
    ws.channel(move |stream| {
        Box::pin(async move {
            // Split the WebSocket stream into a sender (sink) and receiver (stream).
            let (sink, mut stream) = stream.split();

            let waiting = Arc::new(DashMap::<String, UnboundedSender<Value>>::new());

            // Subscribe to updates using the EventChannels' subscribe method.
            // Here, use the tokio mpsc channel instead of the crossbeam_channel.
            let endpoints = events.subscribe("justin".to_string());

            let receive_waiting = waiting.clone();
            // Start the listener task to send updates over the WebSocket.
            rocket::tokio::spawn(async move {
                let mut sink = sink;
                loop {
                    for (command, inner_endpoints) in (*endpoints).clone() {
                        match command {
                            ClientCommand::GetProfile => {
                                if let Ok(sender) = inner_endpoints.receiver.lock().await.try_recv() {
                                    if let Some(tx) = sender.tx.clone() {
                                        receive_waiting.clone().insert(sender.uuid.clone(), tx);
                                    }
                                    
                                    let command = RemoteCommand { id: sender.uuid, command };
                                    let _ = sink.send(ws::Message::text(serde_json::to_string(&command).unwrap())).await;
                                }
                            },
                            ClientCommand::GetFollowers => {
                            },
                            ClientCommand::GetFollowing => {
                            },
                            ClientCommand::GetWebFinger => {
                            },
                            ClientCommand::PostInbox(_) => {
                                if let Ok(sender) = inner_endpoints.receiver.lock().await.try_recv() {
                                    let command = RemoteCommand { id: sender.uuid, command };
                                    let _ = sink.send(ws::Message::text(serde_json::to_string(&command).unwrap())).await;
                                }
                            },
                            ClientCommand::GetOutbox => {
                            },
                        };
                    }

                    let _ = rocket::tokio::time::sleep(rocket::tokio::time::Duration::from_millis(10)).await;
                }
            });


            // Handle incoming messages (You might want to adapt this part based on your needs).
            while let Some(message) = stream.next().await {
                log::debug!("HANDLING INCOMING");

                match message {
                    Ok(msg) => {
                        if let Message::Ping(_) = msg {
                            log::debug!("Ignoring ping message");
                        } else  if let Ok(response) = serde_json::from_str::<RemoteResponse>(&msg.to_string()) {
                            if let Some(socket) = waiting.get(&response.id) {
                                log::debug!("{response:#?}");
                                //let guard = socket.lock().await;
                                let _ = socket.send(response.object);
                                //drop(guard);
                            }
                        }
                    },
                    Err(e) => {
                        eprintln!("Error receiving WebSocket message: {}", e);
                    }
                }
            }

            Ok(())
        })
    })
}

#[post("/inbox", data = "<activity>")]
pub async fn inbox_post(
    //signed: Signed,
    managed: ManagedConfiguration,
    mut events: EventChannels,
    activity: String,
) -> Result<Status, Status> {
    //if let Signed(true) = signed {
        if let Ok(json) = serde_json::from_str::<Value>(&activity) {
            let _ = events.command("justin".to_string(), ClientCommand::PostInbox(json)).await;
            Ok(Status::Accepted)
        } else {
            log::debug!("FAILED TO DECODE JSON\n{activity:#?}");
            Err(Status::BadRequest)
        }
    //} else {
    //    log::debug!("SIGNATURE VERIFICATION FAILED");
    //    Err(Status::BadRequest)
    //}
}

#[get("/outbox")]
pub async fn outbox(
    managed: ManagedConfiguration
) -> Result<Json<Value>, Status> {
    Err(Status::NotImplemented)
}

#[get("/profile")]
pub async fn profile(
    managed: ManagedConfiguration,
    mut events: EventChannels,
) -> Result<Json<Value>, Status> {

    events.command("justin".to_string(), ClientCommand::GetProfile).await
    // events.send("justin".to_string(), "GetProfile".to_string());

    // let mut k = None;
    // let mut tries = 1000;
    
    // while k.is_none() && tries > 0 {
    //     k = events.receive("justin".to_string()).await;
    //     rocket::tokio::time::sleep(rocket::tokio::time::Duration::from_millis(10)).await;
    //     tries -= 1;
    // }

    // log::debug!("K: {k:#?}");
    // Err(Status::NotImplemented)
}

#[get("/objects/<uuid>")]
pub async fn objects(uuid: String) -> Result<Json<Value>, Status> {
    Err(Status::NotImplemented)
}

#[get("/.well-known/webfinger?<resource>", format = "application/jrd+json")]
pub async fn webfinger(
    managed: ManagedConfiguration,
    resource: String,
) -> Result<Json<WebFinger>, Status> {
    Err(Status::NotImplemented)
}

#[get("/.well-known/host-meta")]
pub async fn host_meta(managed: ManagedConfiguration) -> Result<String, Status> {
    Err(Status::NotImplemented)
}

#[get("/.well-known/nodeinfo")]
pub async fn nodeinfo() -> Result<Json<NodeInfo>, Status> {
    Err(Status::NotImplemented)
}
